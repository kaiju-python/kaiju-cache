.. kaiju_tools documentation master file, created by
   sphinx-quickstart on Wed Dec 25 19:20:48 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to kaiju_tools's documentation!
=======================================

.. toctree::
   :maxdepth: 2

   package
   changes
   license


Readme
======

.. include:: ../../README.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
