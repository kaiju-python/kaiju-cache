from .abc import AbstractCache
from .services import *

__version__ = '0.1.0'
__python_version__ = '3.8'
__author__ = 'antonnidhoggr@me.com'
__service_package__ = True
