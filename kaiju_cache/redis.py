from typing import Optional

import aredis

from kaiju_tools.encoding import serializers, MimeTypes
from kaiju_tools.services import ContextableService

from .abc import AbstractCache

__all__ = ['RedisCache', 'RedisClusterCache']


class RedisCache(aredis.StrictRedis, ContextableService, AbstractCache):
    """
    Interface for redis caching backend.

    Basically it's compatible with :class:`.abc.AbstractCache`, but it also
    implements all :class:`aredis.StrictRedis` methods as well.

    .. caution::

        Value type compatibility is achieved using JSON, so ensure that you are
        using JSON compatible types here.

    """

    service_name = 'redis'

    def __init__(self, app, *args, env: str = 'dev', default_serializer=MimeTypes.msgpack, logger=None, **kws):
        ContextableService.__init__(self, app=app, logger=logger)
        aredis.StrictRedis.__init__(self, *args, **kws)
        self._closed = True
        self._serializer = serializers[default_serializer]
        if app:
            self._app_name = self.app.get('name', 'app')
        else:
            self._app_name = 'app'
        self._env = env

    async def init(self):
        await self.info()
        self._closed = False

    async def close(self):
        self._closed = True

    @property
    def closed(self) -> bool:
        return self._closed

    # other AbstractCache methods are present in StrictRedis base class

    async def get(self, key: str, json=True, bytes=False) -> Optional:
        key = self.get_key(key)
        value = await super().get(key)
        if value:
            if json:
                return self._serializer.loads(value)
            else:
                if bytes:
                    return value
                else:
                    return value.decode('utf-8')

    async def mget(self, *keys: str, json=True, bytes=False) -> dict:
        _keys = self.get_keys(keys)
        values = await super().mget(*_keys)
        result = {}
        if json:
            for k, v in zip(keys, values):
                if v is not None:
                    result[k] = self._serializer.loads(v)
        else:
            for k, v in zip(keys, values):
                if v is not None:
                    if not bytes:
                        v.decode('utf-8')
                    result[k] = v
        return result

    async def set(self, key: str, value, ttl: int = None, json=True):
        key = self.get_key(key)
        if json:
            value = self._serializer.dumps_bytes(value)
        elif not isinstance(value, (str, bytes)):
            value = str(value)
        await super().set(key, value)
        if ttl:
            await super().pexpire(key, ttl)

    def mset(self, json=True, **keys):
        _keys = self.get_keys(keys)
        if json:
            keys = {_k: self._serializer.dumps_bytes(v) for _k, (k, v) in zip(_keys, keys.items())}
        else:
            data = {}
            for _k, (k, v) in zip(_keys, keys.items()):
                if not isinstance(v, (str, bytes)):
                    v = str(v)
                data[_k] = v
            keys = data
        return super().mset(**keys)

    def delete(self, *keys: str):
        keys = self.get_keys(keys)
        return super().delete(*keys)

    def exists(self, key: str):
        key = self.get_key(key)
        return super().exists(key)

    def get_keys(self, keys):
        return [self.get_key(key) for key in keys]

    def get_key(self, key):
        return f'{self._env}-{self._app_name}-{key}'


class RedisClusterCache(RedisCache, aredis.StrictRedisCluster):

    def __init__(self, app, *args, default_serializer=MimeTypes.msgpack, logger=None, **kws):
        ContextableService.__init__(self, app=app, logger=logger)
        aredis.StrictRedisCluster.__init__(self, *args, **kws)
        self._closed = True
        self._serializer = serializers[default_serializer]
