import pickle
import marshal
import types

from fastjsonschema import *

from kaiju_tools.jsonschema import Object, String, compile_schema

from .fixtures import *
from ..services import *

_Bin = type('_Bin', (), {})


async def _test_cache(cache, logger):

    logger.info("Testing basic operations")

    key = 'test'
    await cache.set(key, key, ttl=1000000)
    _value = await cache.get(key)
    assert _value == key
    await cache.delete(key)
    _value = await cache.exists(key)
    assert _value is False

    logger.info("Testing bulk operations")

    key1, key2 = 'test1', 'test2'
    await cache.mset(**{key1: key1, key2: key2})
    _value1, _value2 = await cache.mget(key1, key2)
    assert _value1 == key1
    assert _value2 == key2

    logger.info("Testing json dumps")

    await cache.set(key, {'data': True})
    result = await cache.get(key)
    assert result['data'] is True

    logger.info('Testing binary data dumps / loads')
    _obj = _Bin()
    _obj.a = 42
    await cache.set(key, pickle.dumps(_obj), json=False)
    _obj = await cache.get(key, json=False, bytes=True)
    _obj = pickle.loads(_obj)
    assert _obj.a == 42

    _obj = Object({'id': String()})
    _obj = compile_schema(_obj)

    code = marshal.dumps(_obj.__code__)
    await cache.set(key, code, json=False)
    code = await cache.get(key, json=False, bytes=True)
    code = marshal.loads(code)
    _obj = types.FunctionType(code, globals(), "f")
    _obj({'id': 'test'})


async def test_redis_cache(redis, redis_cache, logger):
    await _test_cache(redis_cache, logger=logger)
