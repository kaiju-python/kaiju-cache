"""
Place your pytest fixtures here.
"""

import pytest

from kaiju_tools.docker import DockerContainer
from kaiju_tools.tests.fixtures import *

from ..services import RedisCache, LocalCache

REDIS_PORT = 6399


def _redis_container(logger):
    return DockerContainer(
        image={'tag': 'eqalpha/keydb', 'version': 'latest'},
        name='pytest-redis',
        ports={'6379': str(REDIS_PORT)},
        healthcheck={
            'test': "echo 'INFO' | redis-cli", 'interval': 100000000,
            'timeout': 3000000000, 'start_period': 1000000000, 'retries': 3
        },
        sleep_interval=0.5,
        remove_on_exit=True,
        logger=logger
    )


@pytest.fixture
def redis(logger):
    """
    Returns a new redis container. See `kaiju_tools.tests.fixtures.container`
    for more info.
    """
    with _redis_container(logger) as c:
        yield c


@pytest.fixture(scope='session')
def per_session_redis(logger):
    """
    Returns a new redis container. See `kaiju_tools.tests.fixtures.container`
    for more info.
    """
    with _redis_container(logger) as c:
        yield c


@pytest.fixture
def redis_cache(logger):
    redis = RedisCache(
        app=None, logger=logger,
        host='localhost', port=REDIS_PORT)
    return redis


@pytest.fixture
def local_cache(logger):
    cache = LocalCache(app=None, logger=logger)
    return cache
